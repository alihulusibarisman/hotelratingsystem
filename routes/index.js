'use strict';

const express = require('express');
const router = express.Router();

const async = require("async");
const request = require('request');

const mongoose = require('mongoose');
const database = require('../core/database.js');

const userModel = require('../core/model/UserModel.js');
const SessionModel = require('../core/model/SessionModel.js');
const HotelModel = require('../core/model/HotelModel.js');
const ReviewModel = require('../core/model/ReviewModel.js');
const contactModel = require('../core/model/ContactModel.js');
const validation = require('../core/helper/Validation.js');
const StringUtilities = require('../core/helper/StringUtilities.js');

var sess;

module.exports = function (router) {
    /* GET home page. */
    router.get('/', function (req, res, next) {
        res.send("._.");
    });

    router.post('/forgotPasswordUser', function (req,res) {
        var params = req.body;
        console.log(params);

        validation.validateForgotPassword(params,function (response) {
            console.log(response);
            if (response.success){
                userModel.findByEmail(params.email.toLowerCase(), function (response) {
                    if(response.success){
                        var account = response.account;
                        if(account.email === params.email.toLowerCase()){
                            //içeride
                            var helper = require('sendgrid').mail;
                            var to_email = new helper.Email(account.email);
                            var passwordString = StringUtilities.generatePassword();
                            var from_email = new helper.Email('info@hotelrating.com');
                            var subject = 'New Password';
                            var content = new helper.Content('text/html', "<h2><strong>Your New Password:</strong></h2>" +
                                "<br/> <strong>New Password:</strong> " + passwordString + " ");
                            var mail = new helper.Mail(from_email, subject, to_email, content);
                            var sg = require('sendgrid')("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");
                            var request = sg.emptyRequest({
                                method: 'POST',
                                path: '/v3/mail/send',
                                body: mail.toJSON()
                            });
// With callback
                            sg.API(request, function (error, response) {
                                if (error) {
                                    console.log('Error response received');
                                    res.send({success:false, message:"Şifre mail olarak gönderilemedi.Tekrar deneyin"});
                                }
                                console.log(response.statusCode);
                                console.log(response.body);
                                console.log(response.headers);
                                // user info değişecek
                                // user.password güncellenecek

                                account.password = passwordString;
                                userModel.updateForgotPassword(account.id,account, function (response) {
                                    if(response.success){
                                        res.send({success:true, message:"Password sent as mail."});
                                    }else{
                                        res.send({success:false, message:"Şifre mail olarak gönderilemedi. Tekrar deneyin"});
                                    }
                                })
                            });

                        }
                    }
                    else{
                        //
                        res.send({success:false,message:"Email address is false!"});
                    }
                })
            }
            else {
                res.send({success:false,message:'Please enter valid email address!'});
            }
        })
    });

    router.post('/findUserById', function (req, res) {
        var params = req.body;

        userModel.findByUserId(params.userId, function (response) {
            if (response.success) {
                //TODO:FDSJKFSDH
            }
            else {

            }
        });
    });

    router.post('/loginUser', function (req, res) {
        var params = req.body;
        console.log(params);
        sess=req.session;

        userModel.findByEmail(params.email.toLowerCase(), function (response) {
            if(response.success){
                var account = response.account;
                if(account.password){
                    if(account.password === params.password){
                        //içeride
                        var sessionIDString = StringUtilities.generateSessionID();
                        sess.sessionID = sessionIDString;
                        sess.email=req.body.email;

                        var sessionData = {
                            sessionID:sess.sessionID,
                            userID:account._id
                        };

                        SessionModel.create(sessionData, function (returnValue) {
                            if(returnValue.success){
                                res.send({success:true, message:"Giriş Yapıldı.", account:account});

                            }else{
                                res.send({success:false, message:"Session error"});
                            }
                        });

                    }else{
                        //
                        res.send({success:false,message:"Password is false!"});
                    }
                }
            }
            else{
                res.send({success:false,message:"Please enter valid email address!"});
            }
        })
    });

    router.post('/registerUser', function (req,res) {
        var params = req.body;
        console.log(params);

        validation.validateRegister(params , function (response) {
            if(response.success){
                userModel.create( params,function (userRes) {
                    res.send(userRes);
                    //res.send({success:true, id:userRes.account._id});
                });
            }else{
                console.log(response);
                res.send(response);
            }
        });
    });

    router.post('/user/get',function (req, res) {
        var params = req.body;
        console.log(params);
        SessionModel.findById(params.sessionID, function (response) {
            if(response.success){
                userModel.findByUserId(response.result.userID, function (response) {
                    console.log("response");
                    console.log(response);
                    if (response.success) {
                        res.send({success:true, account:response.result[0]});
                    }else{
                        res.send(response);
                    }
                });
            }else{

            }
        });
    });

    router.post('/user/list', function (req, res) {
        var params = req.body;
        console.log(params);
        userModel.list(function (data) {
            res.send({success:true, data:data});
            // res.send(data);
            console.log("data");
            console.log(data);
        });
    });

    router.post('/user/admin/update',function (req, res) {
        var params = req.body;
        console.log(params);

        userModel.findByUserId(params.userId, function (response) {
            if (response.success) {
                validation.validateUpdateUserAdmin(params,function (response) {
                    console.log(response);
                    console.log("userRes");
                    if(response.success){
                        userModel.updateById(params.userId,params, function (userRes) {
                            console.log("userrrress");
                            console.log(userRes);
                            //res.send(userRes);
                            if(userRes.success){
                                res.send({success:true});
                            }else{
                                res.send({success:false,message:["register error"]});
                            }
                        });
                    }else{
                        console.log(response);
                        res.send({success:false,message:response.message});
                    }
                });


            }else{
                res.send({success:false, message:"User not found!"});
            }
        });

    });

    router.post('/user/update',function (req, res) {
        var params = req.body;
        console.log(params);

        userModel.findByUserId(params.userId, function (response) {
            if (response.success) {
                validation.validateUpdateUser(params,function (response) {
                    console.log(response);
                    console.log("userRes");
                    if(response.success){
                        userModel.updateById(params.userId,params, function (userRes) {
                            console.log("userrrress");
                            console.log(userRes);
                            //res.send(userRes);
                            if(userRes.success){
                                res.send({success:true});
                            }else{
                                res.send({success:false,message:["register error"]});
                            }
                        });
                    }else{
                        console.log(response);
                        res.send({success:false,message:response.message});
                    }
                });


            }else{
                res.send({success:false, message:"User not found!"});
            }
        });

    });

    router.post('/hotel/update',function (req, res) {
        var params = req.body;
        console.log(params);

        HotelModel.findByHotelId(params.hotelId, function (response) {
            if (response.success) {
                validation.validateHotelUpdate(params,function (response) {
                    console.log(response);
                    console.log("userRes");
                    if(response.success){
                        HotelModel.updateById(params.hotelId,params, function (userRes) {
                            console.log("userrrress");
                            console.log(userRes);
                            //res.send(userRes);
                            if(userRes.success){
                                res.send({success:true});
                            }else{
                                res.send({success:false,message:["register error"]});
                            }
                        });
                    }else{
                        console.log(response);
                        res.send({success:false,message:response.message});
                    }
                });


            }else{
                res.send({success:false, message:"Hotel not found!"});
            }
        });

    });

    router.post('/addHotel', function (req,res) {
        var params = req.body;
        console.log(params);

        validation.validateAddHotel(params, function (response) {
            if(response.success){
                HotelModel.create(params, function (hotelRes) {
                    res.send(hotelRes);
                });

            }else{
                console.log(response);
                res.send(response);
            }
        });
    });

    router.post('/user/delete', function (req, res) {
        var params = req.body;
        console.log(params);

        userModel.deleteById(params.id, function (response) {
            console.log(response);
            if (response.success) {
                res.send({success:true });
            }else{
                res.send({success:false, message:"Kullanıcı silinemedi"});
            }
        });
    });

    router.post('/hotel/get',function (req, res) {
        var params = req.body;
        console.log(params);

        HotelModel.findByHotelId(params.hotelId, function (response) {
            console.log("response");
            console.log(response);
            if (response.success) {
                res.send({success:true, account:response.result[0]});
            }else{
                res.send({success:false, message:"Otel bulunamadı!"});
            }
        });
    });

    router.post('/review/get',function (req, res) {
        var params = req.body;
        console.log(params);

        ReviewModel.findByHotelId(params.hotelId, function (response) {
            console.log("response");
            console.log(response);
            if (response.success) {
                res.send({success:true, result:response.result});
            }else{
                res.send({success:false, message:"Review bulunamadı!"});
            }
        });
    });

    router.get('/updateHotelAdmin', function (req, res) {
        HotelModel.findByHotelId(req.query.hotelId, function (response) {
            if(response.success){
                res.send({success:true, result:response.result[0]});
                console.log("success");

                console.log(response.result[0]);
            }else{
                res.send("/hotel hata veriyor");
            }
        });
    });

    router.get('/updateUserAdmin', function (req, res) {
        userModel.findByUserId(req.query.userId, function (response) {
            if(response.success){
                res.send({success:true, result:response.result[0]});
                console.log("success");

                console.log(response.result[0]);
            }else{
                res.send("/user hata veriyor");
            }
        });
    });


    router.post('/hotel/list', function (req, res) {
        var params = req.body;
        console.log("hotelList params");
        console.log(params);
        HotelModel.list(function (data) {
            res.send({success:true, data:data});
            // res.send(data);
            console.log("data");
            console.log(data);
        });
    });

    router.post('/addReview', function (req,res) {
        var params = req.body;
        console.log(params);

        validation.validateAddReview(params, function (response) {
            if(response.success){
                ReviewModel.create(params, function (reviewRes) {
                    res.send(reviewRes);
                });
            }else{
                console.log(response);
                res.send(response);
            }
        });
    });

    /*router.post('/hotels/result', function (req, res) {
        var params = req.body;
        HotelModel.findById(params.hotelId, function (response) {
            if(response.success){
                console.log(params);

                var query = {name: { "$in" : [response.accounts.name] }};
                HotelModel.findHotelsByQuery(query,function (response) {
                    console.log(response);

                    var hotels = [];

                })

            });
        })


    });*/

    router.post('/review/list', function (req, res) {
        var params = req.body;
        console.log("reviewList params");
        console.log(params);
        ReviewModel.list(function (data) {
            res.send({success:true, data:data});
            // res.send(data);
            console.log("data");
            console.log(data);
        });
    });

    router.post('/contactUser', function (req,res) {
        var params = req.body;
        console.log(params);
        console.log("1");
        validation.validateContact(params , function (response) {
            console.log("2");
            console.log(response);
            if(response.success){
                contactModel.create( params,function (contactRes) {
                    var helper = require('sendgrid').mail;
                    var to_email = new helper.Email('hotelrating.ali@gmail.com');
                    var from_email = new helper.Email(params.email);
                    var subject = 'New Message';
                    var content = new helper.Content('text/html', "<h2><strong>New Contact Message:</strong></h2>" +
                        "<br/> <strong>Name:</strong> " + params.name + " "
                        + "<br/> <strong>Email:</strong> " + params.email + " " + "<br/> <strong>Phone:</strong> " +
                        params.phone + " " + "<br/> <strong>Message:</strong> " + params.contactMessage + " ");
                    var mail = new helper.Mail(from_email, subject, to_email, content);
                    var sg = require('sendgrid')("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");
                    var request = sg.emptyRequest({
                        method: 'POST',
                        path: '/v3/mail/send',
                        body: mail.toJSON()
                    });

                    sg.API(request, function(error, response) {
                        console.log(response.statusCode);
                        console.log(response.body);
                        console.log(response.headers);
                        res.send({success:true});
                    });
                });
            }else{
                console.log(response);
                res.send(response);
            }
        });
    });

    return router;
};
