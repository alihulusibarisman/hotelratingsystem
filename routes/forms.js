/**
 * Created by alibarisman on 16/10/2017.
 */
const express = require('express');
const router = express.Router();

const async = require("async");
const request = require('request');

const mongoose = require('mongoose');
const database = require('../core/database.js');

const userModel = require('../core/model/UserModel.js');
const SessionModel = require('../core/model/SessionModel.js');

const validation = require('../core/helper/Validation.js');
const stringUtilities = require('../core/helper/StringUtilities.js');

var sess;

module.exports = function (router) {
    /* GET home page. */

    router.get('/update/profile', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../updateUser.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/user/home', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../index.html?id='+sess.sessionID);
        }else{
            res.redirect('../index.html');
        }
    });

    router.get('/user/logout', function (req, res) {
        sess = req.session;
        req.sessionID = "";
        res.redirect('../index.html');
    });

    router.get('/user/profile', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../main.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/search/result', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../searchResults.html?id='+sess.sessionID);
        }else{
            res.redirect('../searchResults.html');
        }
    });

    router.get('/admin/dashboard', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../adminpanel.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/admin/users', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../adminpanel_users.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/admin/hotels', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../adminpanel_hotels.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/admin/reviews', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../adminpanel_reviews.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/admin/addHotel', function (req, res) {
        sess = req.session;
        if(sess.sessionID){
            res.redirect('../addHotel.html?id='+sess.sessionID);
        }else{
            res.redirect('../login.html');
        }
    });

    router.get('/hotel', function (req,res) {
        var params = req.query;
        console.log(params);

        sess = req.session;

        if(params.name){
            if(sess.sessionID){
                console.log("session var");
                res.redirect('./hotels/'+params.name+'.html?id='+sess.sessionID);
            }else{
                console.log("session yok");
                res.redirect('./hotels/'+params.name+'.html');
            }
        }else{
            res.redirect('/');
        }

    });


    return router;
};

