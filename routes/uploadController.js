'use strict';

const multer = require('multer');
const fs = require('fs');
const path = require('path');
const s3Service = require('../core/helper/s3Service');

module.exports = function (router, isAuthenticated) {
    const uploadPath = path.join(__dirname, '../uploads/');

    const imageFileFilter = function (req, file, cb) {
        var extension = file.originalname.split('.').pop();
        if (extension === 'png') {
            cb(null, true);
        } else if (extension === 'jpg') {
            cb(null, true);
        } else if (extension === 'jpeg') {
            cb(null, true);
        } else {
            cb(null, false);
        }
    };

    var imageStorage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, uploadPath);
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + '.png');
        }
    });

    router.post('/upload', [multer({
        storage: imageStorage,
        fileFilter: imageFileFilter
    }).single('image'), function (req, res) {

        if (req.file === null || req.file === undefined) {
            //Invalid file
            res.send({success: false, message: 'Invalid file'});
        } else {
            var filePath = req.file.path;
            var filename = req.file.filename;

            s3Service.upload(filePath, filename, function (imageUrl) {
                res.send({success: true, path: imageUrl});
            });
        }
    }]);

    return router;
};
