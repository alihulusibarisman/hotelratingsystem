"use strict";

const schemaName = 'review';

module.exports = function (mongoose) {
    var schema = mongoose.Schema(
        {
            hotelId:{type:mongoose.Schema.ObjectId, ref:"hotel"},
            userId:{type:mongoose.Schema.ObjectId, ref:"user"},
            user: {type: String},
            hotel: {type: String},
            comment: {type: String},
            rating: {type: Number},
            status:{type:String, enum:['ACTIVE','PASSIVE'], default:'ACTIVE'}
        },
        {
            toObject: {virtuals: true},
            toJSON: {virtuals: true},
            versionKey: false,
            timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
        }
    );

    return mongoose.model(schemaName, schema);
};
