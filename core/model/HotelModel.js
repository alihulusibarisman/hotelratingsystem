/*jshint esversion: 6 */
'use strict';

const CLASS_TAG = "HotelModel";

var Hotel = require('../database.js').Hotel;

exports.findByHotelId = function (hotelId, callback) {
    Hotel.find({_id:hotelId, status:'ACTIVE'},function (err, result) {
        if(!err){
            callback({success:true, result:result});
        }else{
            callback({success:false, error:"Hotel not found"});
        }
    });
};

exports.findById = function (id, populate, callback) {
    if (typeof populate === 'function') {
        callback = populate;
        populate = undefined;
    }

    var options = null;
    if (populate) {
        options = {populate: populate};
    }

    Hotel.findOne({_id:id}, null, options, function (err, result) {
        if(err){
            callback({success:false})
        }else{
            callback({success:true, account:result});
        }

    });
};

exports.findHotelsByQuery = function (query, callback) {
    Hotel.find(query,null,null,function (error,response) {
        if(!error){
            callback({success:true, result:response});
        }else {
            callback({success:false});
        }
    });
};

exports.create = function (params, callback) {
    checkIfEmailExists(params.email, function (response) {

        console.log("----");
        console.log(response);
        console.log("----");

        if (!response.success) {
            console.log(params);

            var hotel = new Hotel(params);
            hotel.save(function (err, accountItem) {
                if (err) {
                    throw err;
                }
                callback({success: true, account:accountItem});
            });
        }else{
            callback({success: true, account:response.hotel});
        }
    });
};

exports.list = function (callback) {
    find({}, callback);
};

exports.deleteById = function (id, callback) {
    Hotel.remove({_id: id}, function (err) {
        if (err) {
            callback({success: false, error: err});
        } else {
            callback({success: true});
        }
    });
};

exports.findByEmail = function (email, callback) {
    findOne({email: email}, callback);
};


exports.updateById = function (id, args, callback) {
    Hotel.findById(id, function (err, hotel) {
        if (err) return handleError(err);

        hotel.name = args.name;
        hotel.hotelprice = args.hotelprice;
        hotel.facebookaddress = args.facebookaddress;
        hotel.instagramaddress = args.instagramaddress;
        hotel.facilities = args.facilities;
        hotel.status = args.status;

        hotel.save(function (err, updatedHotel) {
            if (err) {
                callback({success: false});
            }else {
                callback({success: true});
            }
        });
    });
};

/**
 * Find with query
 *
 * @param query     Find query
 * @param callback  Callback function
 */
function find(query, callback) {
    Hotel.find(query).exec(function (err, accounts) {
        if (err) {
            callback({success: false, error: err});
        } else {
            callback({success: true, accounts: accounts});
        }
    });
}

/**
 * Find single account with query
 *
 * @param query     Find query
 * @param callback  Callback function
 */
function findOne(query, callback) {
    Hotel.findOne(query).exec(function (err, account) {
        if (err) {
            callback({success: false, error: err});
        } else {
            if (account) {
                console.log(account);
                callback({success: true, account: account});
            } else {
                callback({success: false, message: 'Account not found'});
            }
        }
    });
}


/**
 * Checks if given email already exists in the database
 *
 * @param email     Email address to be checked
 * @param callback  Callback function, returns true if email exists in database, returns false otherwise
 */
function checkIfEmailExists(email, callback) {
    console.log(email);
    console.log("_");
    findOne({email: email}, function (response) {
        console.log(response);
        callback({success:response.success, hotel:response.account});
    });
}
