'use strict';

const CLASS_TAG = "contactModel";

var Contact = require('../database.js').Contact;

exports.create = function (params, callback) {
    console.log(params);

    var contact = new Contact(params);
    contact.save(function (err, contactItem) {
        if(!err){
            callback({success: true, contact: contactItem});
        }else{
            callback({success: false});
        }

    });

};