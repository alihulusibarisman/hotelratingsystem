/*jshint esversion: 6 */
'use strict';

const CLASS_TAG = "userModel";

var User = require('../database.js').User;

/**
 * Find account by email address
 *
 * @param email     Email address of registered account
 * @param callback  Callback function, has response json parameter
 */

exports.findByUserId = function (userId, callback) {
    User.find({_id:userId,status:'ACTIVE'}, function (err, result) {
        if(!err){
            callback({success:true, result:result});
        }else{
            callback({success:false, error:"User not found"});
        }
    });
};

/**
 * Find account by idu7
 *
 * @param id        Id of account
 * @param populate  References to populate
 * @param callback  Callback function, has response json parameter
 */
exports.findById = function (id, populate, callback) {
    if (typeof populate === 'function') {
        callback = populate;
        populate = undefined;
    }

    var options = null;
    if (populate) {
        options = {populate: populate};
    }

    User.findOne({_id:id}, null, options, function (err, result) {
        if(err){
            callback({success:false})
        }else{
            callback({success:true, account:result});
        }

    });
};

exports.findUsersByQuery = function (query, callback) {
    User.find(query,null,null,function (error,response) {
        if(!error){
            callback({success:true, result:response});
        }else {
            callback({success:false});
        }
    });
};
/**
 * Creates new account.
 *
 * @param params    Json object for parameters needed to create new account
 * @param callback  Callback function, returns accountItem
 */
exports.create = function (params, callback) {
    checkIfEmailExists(params.email, function (response) {

        console.log("----");
        console.log(response);
        console.log("----");

        if (!response.success) {
            // TODO: add salt
            console.log(params);

            var user = new User(params);
            user.save(function (err, accountItem) {
                if (err) {
                    throw err;
                    //alert("existing email");
                }
                callback({success: true, account: accountItem});
            });
        }else{
            callback({success: true, account:response.user});
        }
    });
};

/**
 * Lists all sessions
 *
 * @param callback  Callback function, returns all accounts if successful
 */
exports.list = function (callback) {
    find({}, callback);
};

/**
 * Deletes account object with given id
 *
 * @param id        Id of account to delete
 * @param callback  Callback function
 */
exports.deleteById = function (id, callback) {
    User.remove({_id: id}, function (err) {
        if (err) {
            callback({success: false, error: err});
        } else {
            callback({success: true});
        }
    });
};

exports.findByEmail = function (email, callback) {
    findOne({email: email}, callback);
};

/**
 * Update account
 *
 * @param id        Id of account
 * @param query     Update query
 * @param callback  Callback function
 */
exports.updateById = function (id, args, callback) {
    User.findById(id, function (err, user) {
        if (err) return handleError(err);

        user.name = args.name;
        user.email = args.email;
        user.status = args.status;

        user.save(function (err, updatedUser) {
            if (err) {
                callback({success: false});
            }else {
                callback({success: true});
            }
        });
    });
};

exports.updateForgotPassword = function (id, args, callback) {
    User.findById(id, function (err, user) {
        if (err) return handleError(err);

        user.password = args.password;

        user.save(function (err, updatedUser) {
            if (err) {
                callback({success: false});
            }else {
                callback({success: true});
            }
        });
    });
};

/**
 * Find with query
 *
 * @param query     Find query
 * @param callback  Callback function
 */
function find(query, callback) {
    User.find(query).exec(function (err, accounts) {
        if (err) {
            callback({success: false, error: err});
        } else {
            callback({success: true, accounts: accounts});
        }
    });
}

/**
 * Find single account with query
 *
 * @param query     Find query
 * @param callback  Callback function
 */
function findOne(query, callback) {
    User.findOne(query).exec(function (err, account) {
        if (err) {
            callback({success: false, error: err});
        } else {
            if (account) {
                console.log(account);
                callback({success: true, account: account});
            } else {
                callback({success: false, message: 'Account not found'});
            }
        }
    });
}

/**
 * Checks if given email already exists in the database
 *
 * @param email     Email address to be checked
 * @param callback  Callback function, returns true if email exists in database, returns false otherwise
 */
function checkIfEmailExists(email, callback) {
    console.log(email);
    console.log("_");
    findOne({email: email}, function (response) {
        console.log(response);
        callback({success:response.success, user:response.account});
    });
}
