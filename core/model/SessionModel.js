'use strict';

const CLASS_TAG = "SessionModel";

var Session = require('../database.js').Session;

exports.findByUserId = function (userID, callback) {
    Session.find({_id:userID}, function (err, result) {
        if(!err){
            callback({success:true, result:result});
        }else{
            callback({success:false, error:"Bu user bulunamadı"});
        }
    });
};

/**
 * Find account by idu7
 *
 * @param id        Id of account
 * @param populate  References to populate
 * @param callback  Callback function, has response json parameter
 */
exports.findById = function (id, populate, callback) {
    if (typeof populate === 'function') {
        callback = populate;
        populate = undefined;
    }

    var options = null;
    if (populate) {
        options = {populate: populate};
    }

    Session.findOne({sessionID:id}, null, options, function (err, result) {
        if(err){
            callback({success:false});
        }else{
            callback({success:true, result:result});
        }
    });
};

// create
exports.create = function (params, callback) {
    var session = new Session(params);
    session.save(function (err, sessionItem) {
        if (err) {
            callback({success: false, error: err});
        }else{
            callback({success: true, data: sessionItem});
        }
    });
};



