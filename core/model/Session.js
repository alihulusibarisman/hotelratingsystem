"use strict";

const schemaName = 'session';

module.exports = function (mongoose) {
    var schema = mongoose.Schema(
        {
            userID:{type:String, required:true},
            sessionID: {type: String, required: true}
        },
        {
            toObject: {virtuals: true},
            toJSON: {virtuals: true},
            versionKey: false,
            timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
        }
    );

    return mongoose.model(schemaName, schema);
};
