/*jshint esversion: 6 */
'use strict';

const CLASS_TAG = "ReviewModel";

var Review = require('../database.js').Review;

exports.findByReviewId = function (reviewId, callback) {
    Review.find({_id: reviewId, status:'ACTIVE'}, function (err, result) {
        if(!err){
            callback({success:true, result:result});
        }else{
            callback({success:false, error:"Review not found"});
        }
    });
};

exports.findByHotelId = function (hotelId, callback) {
    Review.find({hotelId:hotelId}, function (err, result) {
        if(!err){
            callback({success:true, result:result});
        }else{
            callback({success:false, error:"Review not found"});
        }
    });
};
/*exports.findByUserId = function (userId, callback) {
    Review.find({userId:userId},
        null,
        {
            sort:{updatedAt:-1},
            populate:{
                path:'reviewId',
                model:'review'
            }
        },
        function (err, result) {
        if(!err){
            callback({success:true, result:result});
        }else{
            callback({success:false, error:"User not found"});
        }
    });
};*/

exports.create = function (params, callback) {
    console.log(params);

    var review = new Review({
        hotelId:params.hotelId,
        userId:params.userId,
        user:params.user,
        hotel:params.hotel,
        comment:params.comment,
        rating:params.rating
    });

    review.save(function (err, reviewItem) {
        if (err) {
            callback({success: false, error: err});
        }else{
            callback({success: true, data: reviewItem});
        }
    });

};

exports.findById = function (id, populate, callback) {
    if (typeof populate === 'function') {
        callback = populate;
        populate = undefined;
    }

    var options = null;
    if (populate) {
        options = {populate: populate};
    }

    Review.findOne({_id:id}, null, options, function (err, result) {
        if(err){
            callback({success:false})
        }else{
            callback({success:true, account:result});
        }

    });
};

exports.updateById = function (id, args, callback) {
    Review.findById(id, function (err, review) {
        if (err) return handleError(err);

        review.comment = args.comment;
        review.rating = args.rating;

        review.save(function (err, updatedHotel) {
            if (err) {
                callback({success: false});
            }else {
                callback({success: true});
            }
        });
    });
};

exports.updateStatus = function (id, args, callback) {
    Review.findById(id, function (err, review) {
        if (err) return handleError(err);

        review.status = args.status;

        review.save(function (err, updatedReview) {
            if (err) {
                callback({success: false});
            }else {
                callback({success: true});
            }
        });
    });
};

exports.findReviewsByQuery = function (query, callback) {
    Review.find(query,null,null,function (error,response) {
        if(!error){
            callback({success:true, result:response});
        }else {
            callback({success:false});
        }
    });
};

exports.list = function (callback) {
    find({}, callback);
};
exports.listReview = function (query,callback) {
    find(query, callback);
}

exports.deleteById = function (id, callback) {
    Review.remove({_id: id}, function (err) {
        if (err) {
            callback({success: false, error: err});
        } else {
            callback({success: true});
        }
    });
};

/**
 * Find with query
 *
 * @param query     Find query
 * @param callback  Callback function
 */
function find(query, callback) {
    Review.find(query).exec(function (err, accounts) {
        if (err) {
            callback({success: false, error: err});
        } else {
            callback({success: true, accounts: accounts});
        }
    });
}

