"use strict";

const schemaName = 'user';

module.exports = function (mongoose) {
    var schema = mongoose.Schema(
        {
            name: {type: String, required: true},
            email: {type: String, required: true},
            password: {type: String, required: true},
            type: {type: String, required: true},
            status:{type:String, enum:['ACTIVE','PASSIVE'], default:'ACTIVE'}
        },
        {
            toObject: {virtuals: true},
            toJSON: {virtuals: true},
            versionKey: false,
            timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
        }
    );

    return mongoose.model(schemaName, schema);
};
