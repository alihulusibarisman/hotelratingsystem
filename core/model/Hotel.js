"use strict";

const schemaName = 'hotel';

module.exports = function (mongoose) {
    var schema = mongoose.Schema(
        {
            name: {type: String},
            comboCity: {type: String},
            comboCounty: {type: String},
            address: {type: String},
            email: {type: String},
            phone: {type: String},
            facilities:[],
            mapsiframe: {type: String},
            streetviewiframe: {type: String},
            website: {type: String},
            facebookaddress: {type: String},
            instagramaddress: {type: String},
            hotelprice: {type: String},
            backgroundimage: {type: String},
            smallimage: {type: String},
            image1: {type: String},
            image2: {type: String},
            image3: {type: String},
            image4: {type: String},
            image5: {type: String},
            image6: {type: String},
            image7: {type: String},
            hotelinformation: {type: String},
            searchingurl: {type: String},
            status:{type:String, enum:['ACTIVE','PASSIVE'], default:'ACTIVE'}
        },
        {
            toObject: {virtuals: true},
            toJSON: {virtuals: true},
            versionKey: false,
            timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }
        }
    );

    return mongoose.model(schemaName, schema);
};
