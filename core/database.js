
'use strict';

var bluebird = require('bluebird');
var mongoose = require('mongoose');

// Cache 50 queries for 30 minutes
var cacheOptions = null;
if (process.env.DEBUG) {
    cacheOptions = {
        max: 50,
        maxAge: 1000 * 60 * 30,
        debug: true
    };
} else {
    cacheOptions = {
        max: 50,
        maxAge: 1000 * 60 * 30
    };
}

mongoose.Promise = bluebird;

require('mongoose-cache').install(mongoose, cacheOptions);
mongoose.connect('mongodb://localhost/hotelrating?poolSize=2000', {
    useMongoClient: true
});

var uri = 'mongodb://localhost/hotelrating';

var connectFunction = function () {
    mongoose.connect(uri, {
        server: {
            poolSize: 2000
        }
    });
};

var db = mongoose.connection;

db.on('error', function (err) {
    console.log(err);
});

db.on('disconnected', function () {
    setTimeout(connectFunction, 5000);
});

db.on('timeout', function () {
    setTimeout(connectFunction, 5000);
});

db.once('open', function (callback) {
    console.log('Connected to hotelrating database.');
});

exports.User = require('./model/User')(mongoose);
exports.Session = require('./model/Session')(mongoose);
exports.Contact = require('./model/Contact')(mongoose);
exports.Hotel = require('./model/Hotel')(mongoose);
exports.Review = require('./model/Review')(mongoose);
