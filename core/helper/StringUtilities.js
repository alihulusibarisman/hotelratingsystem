exports.generatePassword = function() {
    return generateString(10);
}

exports.generateSessionID = function () {
    return generateString(24);
}

function generateString(num) {
    var length = num,
        charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}