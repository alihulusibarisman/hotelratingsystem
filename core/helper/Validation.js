function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateWebsite(website) {
    var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    return re.test(website);
}
function passwordLength(data) {
    return (data.length>7)
}
function passwordUpper(password) {
    var re = /[A-Z]/;
    return re.test(password)
}
function  passwordLower(password) {
    var re = /[a-z]/;
    return re.test(password);
}
function passwordNumber(password) {
    var re = /[0-9]/;
    return re.test(password);
}
function validateNotNull(data) {
    return (data.length>=1);
}
function validatePhone(phone) {
    var re = /^\+?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return re.test(phone);
}

exports.validateLogin = function(data, callback){
    var isError = false;
    var message = [];

    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Your email address is invalid.");
        }
    }else{
        isError = true;
        message.push("Please fill in your e-mail address.");
    }

    if(!validateNotNull(data.password)){
        isError = true;
        message.push("Please enter your password.");
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateRegister = function(data, callback){
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];

    if(!validateNotNull(data.name)){
        isError = true;
        message.push("Please enter your name.");
    }
    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Your email address is invalid.");
        }
    }else{
        isError = true;
        message.push("Please fill in your e-mail address.");
    }

    if (validateNotNull(data.password)){
        if(!passwordLength(data.password)){
            isError = true;
            message.push("Your password must contain at least 8 characters.");
        }
        if(!passwordUpper(data.password)){
            isError = true;
            message.push("Your password must contain at least 1 uppercase character (A-Z)!");
        }
        if(!passwordLower(data.password)){
            isError = true;
            message.push("Your password must contain at least 1 lowercase character (a-z)!");
        }
        if(!passwordNumber(data.password)){
            isError = true;
            message.push("Your password must contain at least 1 number (0-9)!");
        }
    }else {
        isError = true;
        message.push("Please enter your password.")
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateForgotPassword= function(data, callback){
    var isError = false;
    var message = [];

    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Your email address is invalid.");
        }
    }else{
        isError = true;
        message.push("Please fill in your e-mail address.");
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};
exports.validateUpdateUser = function(data, callback){
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];
    if(!validateNotNull(data.name)){
        isError = true;
        message.push("Please enter your name.");
    }
    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Your email address is invalid.");
        }
    }else{
        isError = true;
        message.push("Please fill in your e-mail address.");
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateUpdateUserAdmin = function(data, callback){
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];
    if (!validateNotNull(data.status)) {
        isError = true;
        message.push("Please select a status.");
    }
    if(!validateNotNull(data.name)){
        isError = true;
        message.push("Please enter your name.");
    }
    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Your email address is invalid.");
        }
    }else{
        isError = true;
        message.push("Please fill in your e-mail address.");
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateContact = function (data, callback) {
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];

    if(!validateNotNull(data.name)){
        isError = true;
        message.push("Please enter your name.");
    }

    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Please enter valid email address.");
        }
    }else{
        isError = true;
        message.push("Please enter your email address.");
    }

    if(!validateNotNull(data.phone) || !validatePhone(data.phone)){
        isError = true;
        message.push("Please enter valid phone number.");
    }


    if(!validateNotNull(data.contactMessage)){
        isError = true;
        message.push("Please enter your message.");
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateAddHotel = function(data, callback){
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];

    if(!validateNotNull(data.name)){
        isError = true;
        message.push("Please enter hotel's name.");
    }
    if (!validateNotNull(data.comboCity)) {
        isError = true;
        message.push("Please select a city.");
    }
    if (!validateNotNull(data.comboCounty)) {
        isError = true;
        message.push("Please select a county.");
    }
    if(!validateNotNull(data.address)){
        isError = true;
        message.push("Please enter hotel's address.");
    }
    if (validateNotNull(data.email)){
        if (!validateEmail(data.email)){
            isError = true;
            message.push("Email address is invalid.");
        }
    }else{
        isError = true;
        message.push("Please enter e-mail address.");
    }
    if(!validateNotNull(data.phone) || !validatePhone(data.phone)){
        isError = true;
        message.push("Please enter valid phone number.");
    }
    if(!validateNotNull(data.mapsiframe)){
        isError = true;
        message.push("Please enter map code.");
    }
    if(!validateNotNull(data.streetviewiframe)){
        isError = true;
        message.push("Please enter street view code.");
    }
    if (validateNotNull(data.website)){
        if(!validateWebsite(data.website)){
            isError = true;
            message.push("Please enter website.");
        }
    }else {
        isError = false;
        message.push("Please enter website.");
    }
    if (validateNotNull(data.facebookaddress)){
        if(!validateWebsite(data.facebookaddress)){
            isError = true;
            message.push("Please enter valid Facebook address.");
        }
    }else {
        isError = false;
        message.push("Please enter Facebook address.");
    }
    if (validateNotNull(data.instagramaddress)){
        if(!validateWebsite(data.instagramaddress)){
            isError = true;
            message.push("Please enter valid Instagram address.");
        }
    }else {
        isError = false;
        message.push("Please enter Instagram address.");
    }
    if(!validateNotNull(data.hotelprice)){
        isError = true;
        message.push("Please enter price.");
    }
    if(!validateNotNull(data.hotelinformation)){
        isError = true;
        message.push("Please enter information.");
    }

    if(!validateNotNull(data.searchingurl)){
        isError = true;
        message.push("Please enter Searching URL.");
    }


    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateHotelUpdate = function(data, callback){
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];

    if(!validateNotNull(data.hotelprice)){
        isError = true;
        message.push("Please enter price.");
    }
    if(!validateNotNull(data.name)){
        isError = true;
        message.push("Please enter hotel's name.");
    }
    if (!validateNotNull(data.status)) {
        isError = true;
        message.push("Please select status.");
    }
    if(!validateNotNull(data.facebookaddress)){
        isError = true;
        message.push("Please enter Facebook address.");
    }
    if (!validateNotNull(data.instagramaddress)){
        isError = true;
        message.push("Please enter Instagram address.");
    }


    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

exports.validateAddReview = function(data, callback){
    console.log("data");
    console.log(data);
    var isError = false;
    var message = [];

    if(!validateNotNull(data.comment)){
        isError = true;
        message.push("Comment and rating are required.");
    }

    if(isError){
        callback({success: false, message: message});
    }else{
        message.push("valid");
        callback({success: true, message:message});
    }
};

