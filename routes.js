'use strict';

var express = require('express');
var router = express.Router();

module.exports = function () {
    router = require('./routes/index')(router);
    router = require('./routes/forms')(router);
    router = require('./routes/uploadController')(router);
    return router;
};
