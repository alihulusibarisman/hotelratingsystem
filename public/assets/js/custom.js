var ilceNameArr = [];
var plakaArr = [];
var ilceID = [];

$(document).ready(function(){
	generateIlceList();
});

function generateIlceList(){
	for (var i = 0; i < ilceArr.length; i++) {
		var str = ilceArr[i];
		var arr = str.split(",");
		ilceNameArr.push(arr[2]);
		ilceID.push(arr[1]);
		plakaArr.push(arr[0]);
	};
}

function findIlceWithIlID(idNum){
	var arr = [];

	for (var i = 0; i < ilceArr.length; i++) {
		if(plakaArr[i]==idNum){
			arr.push(ilceArr[i]);
		}	
	}

	return arr;
}

function checkAtLeastOne(){
    var checked=0;
    var isTrue = true;
    $( "input:checkbox" ).each(function() {
        if($(this).is(':checked')){
            checked++;
        }
    });
    if(checked>0){
        isTrue = true;
    }
    else{
        isTrue = false;
    }
    return isTrue;
}

function isEmpty(item) {
    var checkTexts = true;
    for(var i = 0;i<item.length;i++){
        if (!item[i].val()){
            checkTexts = false;
        }
    }
    return checkTexts;
}
