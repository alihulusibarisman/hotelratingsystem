function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


var isUser = false;
var sessionID;

$(document).ready(function(){
    sessionID = getParameterByName('id');

    if(sessionID){
        // alert(sessionID);
        isUser = true;
    }else{
        isUser = false;
    }

    if(!isUser)
    {
        $('#userdiv').show();
        $('#notlogin').show();
        $('#login').hide();
        $('#review').hide();
        $('#indexlogin').show();
        $('#indexlogout').hide();


    }else{
        $('#userdiv').hide();
        $('#notlogin').hide();
        $('#login').show();
        $('#review').show();
        $('#indexlogin').hide();
        $('#indexlogout').show();

    }
});
